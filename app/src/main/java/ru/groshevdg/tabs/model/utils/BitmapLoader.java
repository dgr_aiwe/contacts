package ru.groshevdg.tabs.model.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import java.io.IOException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.groshevdg.tabs.R;

public class BitmapLoader {

    public static Bitmap getBitmapWithSize(Context context, Uri uri) {
        InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                uri, true);
        Bitmap photo = BitmapFactory.decodeStream(is);
        if (photo != null) {
            return photo;
        }
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
    }

    public static void load(Context context, ImageView imageView, Uri url) {
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.background)
                    .into(imageView);
    }


    public static void load(Context context, CircleImageView imageView, Uri url) {
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.background)
                    .into(imageView);
    }
}
