package ru.groshevdg.tabs.model.utils;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.InputStream;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.model.ContactModel;
import ru.groshevdg.tabs.view.fragments.ContactsFragment;

public class ContactsListProvider {

    private Context context;
    private ArrayList<Contact> contactList;
    private ContactModel model;
    private Map<String, String> mobileNumbers;
    private Map<String, String> workNumbers;

    public ContactsListProvider(Context context, ContactModel model) {
        this.context = context;
        this.model = model;
    }

    public void loadContactList() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                contactList = new ArrayList<>();
                workNumbers = new HashMap<>();
                mobileNumbers = new HashMap<>();

                createContacts();
                setContactsEmail();
                setMobileNumbers();
                setWorkNumbers();

                Log.d("Debug", "Contacts were read for " + (System.currentTimeMillis() - startTime) + " millis");

                sortContacts();
                defineFirstLetters();
                model.contactListLoaded();
                Log.d("Debug", "notify model that contact list was read");
            }
        });
    }

    private void createContacts() {
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        while (cursor.moveToNext()) {
            Contact contact = new Contact();

            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Data._ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
            Uri photoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));

            if (name != null) {
                contact.setId(Long.parseLong(id));
                contact.setName(name);
                contact.setPhotoUri(photoUri);
                contactList.add(contact);
            }
        }
    }

    private void sortContacts() {
        Collections.sort(contactList, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
    }

    private void setWorkNumbers() {
        Cursor workNumbersCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[] {ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER, ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
                ContactsContract.CommonDataKinds.Phone.TYPE + " = ?", new String[] {String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_WORK)}, null);

        while (workNumbersCursor.moveToNext()) {

            String id = workNumbersCursor.getString(workNumbersCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            String notFormattedPhoneString = workNumbersCursor.getString(workNumbersCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

            if (notFormattedPhoneString != null) {
                String workNumber = PhoneNumberUtils.formatNumber(notFormattedPhoneString, Locale.getDefault().getCountry());
                workNumbers.put(workNumber, id);
            }
        }

        for (Contact contact: contactList) {
            Iterator<Map.Entry<String, String>> iterator = workNumbers.entrySet().iterator();
            StringBuilder builder = new StringBuilder();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (contact.getId() == Long.parseLong(entry.getValue())) {
                    builder.append(entry.getKey());
                    iterator.remove();
                }
            }
            if (builder.length() != 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
            contact.setWorkNumber(builder.toString());
        }
    }

    private void setMobileNumbers() {
        Cursor mobileNumberCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[] {ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER, ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
                ContactsContract.CommonDataKinds.Phone.TYPE + " = ?", new String[] {String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)}, null);
        while (mobileNumberCursor.moveToNext()) {

            String id = mobileNumberCursor.getString(mobileNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            String notFormattedPhoneString = mobileNumberCursor.getString(mobileNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

            if (notFormattedPhoneString != null) {
                String mobileNumber = PhoneNumberUtils.formatNumber(notFormattedPhoneString, Locale.getDefault().getCountry());
                mobileNumbers.put(mobileNumber, id);
            }
        }

        for (Contact contact: contactList) {
            Iterator<Map.Entry<String, String>> iterator = mobileNumbers.entrySet().iterator();
            StringBuilder builder = new StringBuilder();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (contact.getId() == Long.parseLong(entry.getValue())) {
                    builder.append(entry.getKey() + "\n");
                    iterator.remove();
                }
            }
            if (builder.length() != 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
            contact.setMobileNumber(builder.toString());
        }
    }

    private void setContactsEmail() {
        Cursor emailCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                new String[] {ContactsContract.CommonDataKinds.Email.ADDRESS, ContactsContract.CommonDataKinds.Email.CONTACT_ID}, null, null, null);

        while (emailCursor.moveToNext()) {
            String id = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
            Contact contact = getContactById(Long.parseLong(id));
            String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            contact.setEmail(email);
        }
    }

    private Contact getContactById(long id) {
        for (Contact contact: contactList) {
            if (contact.getId() == id) {
                return contact;
            }
        }
        return null;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    private void defineFirstLetters() {
        String currentLetter = "";
        for (Contact contact : contactList) {
            if (!currentLetter.equals(String.valueOf(contact.getName().charAt(0)).toUpperCase())) {
                currentLetter = String.valueOf(contact.getName().charAt(0)).toUpperCase();
                contact.setFirstLetter(currentLetter);
            }
        }
    }
}
