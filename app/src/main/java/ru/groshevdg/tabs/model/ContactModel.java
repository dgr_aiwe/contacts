package ru.groshevdg.tabs.model;

import android.content.Context;
import android.util.Log;

import java.util.List;

import ru.groshevdg.tabs.contract.Contract;
import ru.groshevdg.tabs.contract.OnModelDataChangedListener;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.model.utils.ContactsListProvider;

public class ContactModel implements Contract.Model {
    private Context context;
    private ContactsListProvider contactsListProvider;
    private OnModelDataChangedListener listener;

    public ContactModel(Context context,  OnModelDataChangedListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void loadContacts() {
        Log.d("Debug", "created new thread");
        contactsListProvider = new ContactsListProvider(context, this);
        contactsListProvider.loadContactList();

    }

    public void contactListLoaded() {
        List<Contact> contactList = contactsListProvider.getContactList();
        listener.dataLoaded(contactList);
    }
}
