package ru.groshevdg.tabs.view.fragments;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.contract.OnDestroyDetailsFragmentListener;
import ru.groshevdg.tabs.model.utils.BitmapLoader;
import ru.groshevdg.tabs.model.utils.Contact;

public class DetailsFragment extends Fragment  {
    private ImageView photo;
    private Toolbar toolbar;
    private TextView mobileNumber;
    private TextView workNumber;
    private TextView email;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS );

        Bundle arguments = getArguments();
        Contact contact = (Contact) arguments.get("contact");

        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(contact.getName());

        photo = view.findViewById(R.id.photo_view);
        photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
        BitmapLoader.load(getContext(), photo, contact.getPhotoUri());

        mobileNumber = view.findViewById(R.id.phone_number);
        mobileNumber.setText(contact.getMobileNumber());

        workNumber = view.findViewById(R.id.work_number);
        workNumber.setText(contact.getWorkNumber());

        email = view.findViewById(R.id.email);
        email.setText(contact.getEmail());

        setHasOptionsMenu(true);

        return view;
    }
}
