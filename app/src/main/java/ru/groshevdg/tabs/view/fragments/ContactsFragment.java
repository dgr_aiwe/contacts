package ru.groshevdg.tabs.view.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.contract.Contract;
import ru.groshevdg.tabs.contract.OnContactWasSelectedListener;
import ru.groshevdg.tabs.contract.OnTabWasClickedListener;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.presenter.ContactsPresenter;
import ru.groshevdg.tabs.presenter.adapters.ContactsRecycleAdapter;

public class ContactsFragment extends Fragment implements Contract.View {
    private ContactsPresenter presenter;
    private ContactsRecycleAdapter adapter;
    private ProgressBar progress;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ContactsPresenter(getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Debug", "view contact's fragment (with recycle view) is created");

        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        progress = view.findViewById(R.id.progress);
        refreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        recyclerView = view.findViewById(R.id.contacts_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
            }
        }

        return view;
    }

    @Override
    public void showContactList(List<Contact> contactList) {
            adapter = new ContactsRecycleAdapter(contactList, (OnContactWasSelectedListener) getActivity());
            recyclerView.setAdapter(adapter);

    }

    @Override
    public void hideRefreshing() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            presenter.getContactsList();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void activityWasCreated() {
        presenter.getContactsList();
    }

    @Override
    public void setProgressVisibility(Boolean value) {
        if (value) progress.setVisibility(View.VISIBLE);
        else progress.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        activityWasCreated();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("Debug", "Swipe activated");
                presenter.refreshContacts();
                adapter.deleteAllContacts();
                refreshLayout.setRefreshing(false);
            }
        });
    }


    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}
