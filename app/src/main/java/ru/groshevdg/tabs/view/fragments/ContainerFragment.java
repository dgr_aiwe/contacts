package ru.groshevdg.tabs.view.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.contract.Contract;
import ru.groshevdg.tabs.contract.OnContactWasSelectedListener;
import ru.groshevdg.tabs.contract.OnTabWasClickedListener;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.presenter.ContactsPresenter;
import ru.groshevdg.tabs.presenter.adapters.CustomViewPagerAdapter;

public class ContainerFragment extends Fragment  {

    private ViewPager viewPager;
    private TabLayout tabs;
    private CustomViewPagerAdapter sectionsPagerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sectionsPagerAdapter = new CustomViewPagerAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        viewPager = view.findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.setTabIconTint(ColorStateList.valueOf(Color.WHITE));
        tabs.getTabAt(0).setIcon(R.drawable.ic_star);
        tabs.getTabAt(1).setIcon(R.drawable.ic_access_time);
        tabs.getTabAt(2).setIcon(R.drawable.ic_people_black);


        viewPager.setCurrentItem(2);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    sectionsPagerAdapter.getContactsFragment().getRecyclerView().scrollToPosition(0);
                }
            }
        });
    }
}
