package ru.groshevdg.tabs.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.model.utils.BitmapLoader;


public class DetailsActivity extends AppCompatActivity {
    private TextView nameView;
    private TextView mobilePhoneView;
    private TextView workPhoneView;
    private TextView emailView;
    private ImageView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_relative_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nameView = findViewById(R.id.name);
        mobilePhoneView = findViewById(R.id.phone_number);
        workPhoneView = findViewById(R.id.work_number);
        emailView = findViewById(R.id.email);
        photoView = findViewById(R.id.photo_view);
        photoView.setScaleType(ImageView.ScaleType.FIT_XY);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        String mobileNumber = intent.getStringExtra("mobileNumber");
        String workNumber = intent.getStringExtra("workNumber");
        String photoUri = intent.getStringExtra("photoUri");

        getSupportActionBar().setTitle(name);

        BitmapLoader.load(this, photoView, Uri.parse(photoUri));
        emailView.setText(email);
        mobilePhoneView.setText(mobileNumber);
        workPhoneView.setText(workNumber);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
}
