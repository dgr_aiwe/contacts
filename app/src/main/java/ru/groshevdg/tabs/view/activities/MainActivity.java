package ru.groshevdg.tabs.view.activities;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

import ru.groshevdg.tabs.contract.Contract;
import ru.groshevdg.tabs.contract.OnContactWasSelectedListener;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.presenter.ContactsPresenter;
import ru.groshevdg.tabs.presenter.adapters.ContactsRecycleAdapter;
import ru.groshevdg.tabs.presenter.adapters.CustomViewPagerAdapter;
import ru.groshevdg.tabs.view.fragments.ContainerFragment;
import ru.groshevdg.tabs.view.fragments.DetailsFragment;


public class MainActivity extends AppCompatActivity implements OnContactWasSelectedListener {
    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);

        getSupportFragmentManager().beginTransaction()
                .add(container.getId(), new ContainerFragment())
                .commit();
    }

    @Override
    public void contactWasSelected(Contact contact) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("contact", contact);

        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(container.getId(), fragment)
                .addToBackStack(null)
                .commit();

    }
}