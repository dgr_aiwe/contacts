package ru.groshevdg.tabs.presenter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.List;

import ru.groshevdg.tabs.contract.Contract;
import ru.groshevdg.tabs.contract.OnModelDataChangedListener;
import ru.groshevdg.tabs.model.ContactModel;
import ru.groshevdg.tabs.model.utils.Contact;

public class ContactsPresenter implements Contract.Presenter, OnModelDataChangedListener {

    private Contract.View view;
    private ContactModel model;
    private Context context;
    private Handler handler;
    private List<Contact> contactList;

    public ContactsPresenter(Context context, Contract.View view) {
        this.view = view;
        this.context = context;
        model = new ContactModel(context, this);
    }

    @Override
    public void getContactsList() {
        if (contactList == null) {
            view.setProgressVisibility(true);
            handler = new Handler();
            Log.d("Debug", "start loading data");
            model.loadContacts();
        }
        else {
            Log.d("Debug", "Presenter have already had data");
            view.setProgressVisibility(false);
            view.showContactList(contactList);
        }
    }

   public void sendContactListToPresenter(final List<Contact> contactList) {
        this.contactList = contactList;
        handler.post(new Runnable() {

            @Override
            public void run() {
                view.showContactList(contactList);
                view.setProgressVisibility(false);
                view.hideRefreshing();
            }
        });
    }

    @Override
    public void dataLoaded(List<Contact> contactList) {
        Log.d("Debug", "Callback to presenter");
        sendContactListToPresenter(contactList);
    }

    public void refreshContacts() {
        contactList = null;
        getContactsList();
    }
}
