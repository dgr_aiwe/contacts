package ru.groshevdg.tabs.presenter.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.groshevdg.tabs.contract.OnContactWasSelectedListener;
import ru.groshevdg.tabs.model.utils.BitmapLoader;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.R;

public class ContactsRecycleAdapter extends RecyclerView.Adapter<ContactsRecycleAdapter.RecycleViewHolder> {
    private List<Contact> contactList;
    private OnContactWasSelectedListener listener;

    public static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private CircleImageView photoView;
        private TextView displayLetter;


        public RecycleViewHolder(View view) {
            super(view);
            nameView = view.findViewById(R.id.contact_name);
            photoView = view.findViewById(R.id.photo);
            displayLetter = view.findViewById(R.id.display_letter);
        }
    }

    public ContactsRecycleAdapter(List<Contact> contacts, OnContactWasSelectedListener activity) {
        this.contactList = contacts;
        listener = activity;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_list, parent, false);
        return new RecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecycleViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactWasSelected(contactList.get(position));
            }
        });
        holder.nameView.setText(contactList.get(position).getName());
        holder.displayLetter.setText(contactList.get(position).getFirstLetter());

        BitmapLoader.load(holder.itemView.getContext(), holder.photoView, contactList.get(position).getPhotoUri());
    }

    @Override
    public int getItemCount() {
        if (contactList != null) {
            return contactList.size();
        }
        else {
            return 0;
        }
    }

    public void deleteAllContacts() {
        contactList.clear();
        this.notifyDataSetChanged();
    }

    public void insertContacts(List<Contact> list) {
        contactList = list;
        this.notifyDataSetChanged();
    }
}
