package ru.groshevdg.tabs.presenter.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import java.util.LinkedList;
import java.util.List;

import ru.groshevdg.tabs.R;
import ru.groshevdg.tabs.contract.OnContactWasSelectedListener;
import ru.groshevdg.tabs.model.utils.Contact;
import ru.groshevdg.tabs.view.fragments.ContactsFragment;
import ru.groshevdg.tabs.view.fragments.DetailsFragment;
import ru.groshevdg.tabs.view.fragments.SecondFragment;
import ru.groshevdg.tabs.view.fragments.ThirdFragment;

public class CustomViewPagerAdapter extends FragmentPagerAdapter {
    private SecondFragment secondFragment;
    private ContactsFragment contactsFragment;


    public CustomViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (secondFragment == null) {
                    return new SecondFragment();
                }
                else {
                    return secondFragment;
                }

            case 1:
                if (secondFragment == null) {
                    return new SecondFragment();
                }
                else {
                    return secondFragment;
                }

            case 2:
                if (contactsFragment == null) {
                    Log.d("Debug", "Created new contacts fragment in view pager");

                    contactsFragment = new ContactsFragment();
                    return contactsFragment;
                }
                else {
                    return contactsFragment;
                }
        }
        return null;
    }


    @Override
    public int getCount() {
        return 3;
    }

    public ContactsFragment getContactsFragment() {
        return contactsFragment;
    }
}
