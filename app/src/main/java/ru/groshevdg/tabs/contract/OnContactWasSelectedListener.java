package ru.groshevdg.tabs.contract;

import ru.groshevdg.tabs.model.utils.Contact;

public interface OnContactWasSelectedListener {
    void contactWasSelected(Contact contact);
}
