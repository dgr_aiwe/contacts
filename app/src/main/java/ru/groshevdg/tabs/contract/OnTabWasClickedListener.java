package ru.groshevdg.tabs.contract;

public interface OnTabWasClickedListener {
    void tabWasClicked();
}
