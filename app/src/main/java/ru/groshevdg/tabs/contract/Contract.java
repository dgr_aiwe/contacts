package ru.groshevdg.tabs.contract;

import java.util.List;

import ru.groshevdg.tabs.model.utils.Contact;

public interface Contract {
    interface View {
        void showContactList(List<Contact> contactList);
        void setProgressVisibility(Boolean value);
        void hideRefreshing();
    }

    interface Presenter {
        void getContactsList();
    }

    interface Model {
        void loadContacts();
    }
}
