package ru.groshevdg.tabs.contract;

import java.util.List;

import ru.groshevdg.tabs.model.utils.Contact;

public interface OnModelDataChangedListener {
    void dataLoaded(List<Contact> contactList);
}
